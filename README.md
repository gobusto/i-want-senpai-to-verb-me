I WANT SENPAI TO VERB ME
========================

notice me senpai uwu

Media used
----------

The background is a modified version of this photo from Wikimedia commons:

<https://commons.wikimedia.org/wiki/File:Chokai_JHS_2B_classroom_2.jpg>
([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/deed.en))

The character image is also from Wikimedia commons:

<https://commons.wikimedia.org/wiki/File:Ifme-chan_drawn_by_ray-en_(small_to_20_percent).png>
([CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en))

The voice sound effect is by lemonjolly on freesound.org:

<https://freesound.org/people/lemonjolly/sounds/273924/>
([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/deed.en))

License
-------

Copyright 2019 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
