;(function () {
  'use strict'

  const sentences = [
    "OK! I'm gonna do it! I'm gonna get $NAME to $VERB me!",
    "There's got to be a way! Somehow, I'm going to make $NAME $VERB me!",
    "I can't give up yet! Not until I get $NAME to $VERB me!"
  ]

  const names = [
    'Doug Dimmadome, Owner of the Dimmsdale Dimmadome',
    'Dunkan85',
    'Love Interest-kun',
    'onee-san',
    'onii-san',
    'sempai',
    'senpai',
    'sensei',
    'the average anilist user',
    'the entire population of Japan'
  ]

  const verbs = [
    'annul',
    'anthropomorphise',
    'be honest with',
    'compartmentalise',
    'defeat',
    'devalue',
    'do my homework for',
    'dream of',
    'duel against',
    'eject',
    'elongate',
    'encapsulate',
    'fight',
    'grow artificial organs in a lab, and surgically implant them into',
    'hang out with',
    'imitate',
    'joke about',
    'kick',
    'love',
    'maim',
    'notice',
    'obsess over',
    'pilot a giant mecha with',
    'play minecraft with',
    'psychoanalyse',
    'quote',
    'remember',
    'repurpose',
    'revitalise',
    'rue crossing',
    'say "TRANS RIGHTS" with',
    'shitpost on anilist.co, just like',
    'sing to',
    'think lewd thoughts about',
    'understand',
    'visit',
    'watch an. i.',
    'xray',
    'yeet',
    'zap'
  ]

  function randomChoice(a) { return a[Math.floor(Math.random() * a.length)] }

  function generate () {
    let text = randomChoice(sentences)
    text = text.replace('$NAME', randomChoice(names))
    text = text.replace('$VERB', randomChoice(verbs))
    document.getElementById("text-window").innerHTML = text
  }

  document.querySelector("button").onclick = function () {
    generate()
    document.querySelector("audio").play()
  }

  generate()
})()
